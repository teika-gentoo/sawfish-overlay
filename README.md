# Sawfish Overlay

This is a Gentoo portage [overlay](https://wiki.gentoo.org/wiki/Ebuild_repository) for the [Sawfish window manager](https://github.com/SawfishWM) and its dependencies. Sawfish is *still alive!*

Until [last-rited](https://bugs.gentoo.org/637978) in Jan 2023, sawfish was in the Gentoo portage tree as ``x11-wm/sawfish``.

## Ebuild description
Ebuilds here simply get the upstream snapshot tarball. *Make sure* these ebuilds don't do anything wrong, *or your system will easily be compromised!*

## How to use
1. [Download the tarball](https://gitlab.com/teika-gentoo/sawfish-overlay/-/archive/master/sawfish-overlay-master.tar.bz2), or clone by:

    `$ git clone git@gitlab.com:teika-gentoo/sawfish-overlay.git`
2. Set the overlay name, which is "sawfish" by default, by modifying the file

    `<overlay root>/profiles/repo_name`
3. Register the sawfish overlay to your Gentoo box, typically adding the lines below to `/etc/portage/repos.conf/gentoo.conf`:

    `[<overlay name>]`

    `location = <path/to/the/overlay>`

    Read [this wiki page](https://wiki.gentoo.org/wiki//etc/portage/repos.conf) for details.

## Latest
Last updated on 2023-06-11.

* x11-wm/sawfish is the upstream snapshots of May 2023.
* x11-libs/rep-gtk is the May 2023 snapshot.
* dev-libs/librep is 0.92.7-r2, the last Gentoo official one, which has Dec 2022 patches.

### Changes
* 2023-06: Sawfish now supports (very minimally) _NET_WM_STRUT_PARTIAL. This can improve some panel / dock type windows support. See [the commit message](https://github.com/SawfishWM/sawfish/commit/8a58cba437bc664e2e482d3a87760d9a54c06735) for details.
* 2023-05: Minor changes: (EAPI 6 -> 8. Better compilation support for rep-gtk.)

## License
These ebuilds are based upon the official ebuilds, which are by Gentoo authors, and under GPLv2. My modifications are given to the public domain.
